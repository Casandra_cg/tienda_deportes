@if(kvfj(Auth::user()->permissions, 'productos'))
@extends('admin.master')

@section('title','Editar Productos')

@section('breadcrumb')
<li class="breadcrumb-item">
	<a href="{{ url('/admin/products') }}"><i class="fas fa-boxes"></i> Productos</a>
</li>
<li class="breadcrumb-item">
	<a href="{{ url('/admin/product/add') }}"><i class="fas fa-pen"></i>Editar producto</a>
</li>
@endsection
@section('content')
	

<div class="container-fluid">
	<div class="row">
		<div class="col-md-9">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="fas fa-pen"></i></i>Editar producto
					</h2>
				</div>
				<div class="inside">
					{!! Form::open(['url' => '/admin/product/'.$p->id.'/edit', 'files' => true]) !!}
					<div class="row">
						<div class="col-md-5">
							<label for="name">Nombre del producto:</label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1"><i class="far fa-keyboard"></i></span>
								{!! Form::text('name', $p->name, ['class' => 'form-control', 'required']) !!}
							</div>
						</div>
						<div class="col-md-3">
							<label for="category">Categoria:</label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1"><i class="fab fa-product-hunt"></i></span>
								{!! Form::select('category', $cats, $p->category_id,  ['class'=> 'form-select', 'required']) !!}
							</div>
						</div>
						<div class="col-md-2">
							<label for="imagen">Imagen:</label>
							<div class="custom-file">
								{!! Form::file('imagen', ['class' => 'custom-file-input', 'id' => 'customFile',  'accept' => 'image/*']) !!}
								<label class="custom-file-label" for="customFile">Imagen</label>
							</div>
						</div>
						<div class="col-md-2">
							<label for="imagen2">Imagen 2:</label>
							<div class="custom-file">
								{!! Form::file('imagen2', ['class' => 'custom-file-input', 'id' => 'customFile',  'accept' => 'image/*']) !!}
								<label class="custom-file-label" for="customFile">Imagen1</label>
							</div>
						</div>
					</div>
					<div class="row mtop16">
						<div class="col-md-3">
							<label for="price">Precio:</label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								{!! Form::number('price', $p->price, ['class' => 'form-control', 'min' => '0.00', 'step' => 'any', 'required']) !!}
							</div>
						</div>
						<div class="col-md-3">
							<label for="indiscount">¿En descuento?</label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								{!! Form::select('indiscount', ['0'=> 'No', '1' => 'Si'], $p->in_discount, ['class'=> 'form-select', 'required']) !!}
							</div>
						</div>
						<div class="col-md-3">
							<label for="discount">Descuento (En decimales):</label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								{!! Form::number('discount', $p->discount, ['class' => 'form-control', 'required','min' => '0.00', 'step' => 'any', 'required']) !!}
							</div>
						</div>
						<div class="col-md-3">
							<label for="indiscount">Estado:</label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								{!! Form::select('status', ['0'=> 'Borrador', '1' => 'Publico'], $p->status, ['class'=> 'form-select', 'required']) !!}
							</div>
						</div>
					</div>
					<div class="row mtop16">
						<div class="col-md-12">
							<label for="content">Descripcion:</label>
							{!!  Form::textarea('content', $p->contenido , ['class' => 'form-control', 'required','id'=>'editor']) !!}
						</div>
					</div>
					<div class="row mtop16">
						<div class="col-md-12">
							{!! Form::submit('Guardar', ['class' => 'btn btn-info']) !!}
						</div>
					</div>
					{!! Form::close() !!}
					@if(Session::has('message'))
					<div class="container">
						<div class="alert alert-{{ Session::get('typealert')}}" style="display:none;">
							{{ Session::get('message')}}
							@if ($errors->any())
							<ul>
								@foreach($errors->all() as $error)
								<li>{{$error}}</li>
								@endforeach
							</ul>
							@endif<script>
								$('.alert').slideDown();
								setTimeout(function(){ $('.alert').slideUp();}, 10000);
							</script>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="fas fa-image"></i>Imagen destacada
					</h2>
					<div class="inside">
						<img src="{{ url('/uploads/'. $p->file_path.'/'.$p->image) }}"  class="img-fluid" >
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@endif