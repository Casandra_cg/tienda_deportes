@if(kvfj(Auth::user()->permissions, 'categorias'))
@extends('admin.master')
@section('title','Categorias')

@section('breadcrumb')
<li class="breadcrumb-item">
	<a href="{{ url('/admin/categories') }}"><i class="far fa-folder-open"></i> Categorias</a>
</li>

@endsection

@section('content')
@if(Session::has('message'))
	<div class="container">
		<div class="alert alert-{{ Session::get('typealert')}}" style="display:none;">
			{{ Session::get('message')}}
			@if ($errors->any())
			<ul>
				@foreach($errors->all() as $error)
				<li>{{$error}}</li>
				@endforeach
			</ul>
			@endif
			<script>
				$('.alert').slideDown();
				setTimeout(function(){$('.alert').slideUp();}, 10000);
			</script>
		</div>
	</div>
	@endif
<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-3">
			<div class="panel shadow">
				
				<div class="header">
					<h2 class="title">
						<i class="fas fa-plus"></i>Agregar categoria
					</h2>
				</div>
				<div class="inside">


					{!! Form::open(['url' => '/admin/category/add']) !!}

					<label for="name">Nombre de la categoria:</label>
					<div class="input-group">
						<span class="input-group-text" id="basic-addon1"><i class="far fa-keyboard"></i></span>

					{!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
					</div>

					<label for="module" class="mtop16">Modulo:</label>
					<div class="input-group">
						<span class="input-group-text" id="basic-addon1"><i class="fab fa-product-hunt"></i></span>

					{!! Form::select('module', getModulesArray() , 0, ['class'=> 'form-select']) !!}
					</div>

					<label for="icono" class="mtop16">Icono:</label>
					<div class="input-group">
						<span class="input-group-text" id="basic-addon1"><i class="fas fa-icons"></i></span>
						{!! Form::text('icono', null, ['class' => 'form-control','required']) !!}
					</div>

					{!! Form::submit('Guardar', ['class' => 'btn btn-info mtop16']) !!}
					{!! Form::close() !!}

				</div>
				
			</div>
		</div>

		<div class="col-md-9">
			<div class="panel shadow">
				
				<div class="header">
					<h2 class="title">
						<i class="far fa-folder-open"></i> Categorias
					</h2>
				</div>
				<div class="inside">
					<nav class="nav nav-pills nav-fill">
						@foreach(getModulesArray()  as $m => $k)
						<a class="nav-link" href="{{ url('/admin/categories/'.$m) }}">{{$k}}</a>
						@endforeach
					</nav>
					<table class="table">
						
						<thead>
							<tr>
								<td width="32"></td>
								<td>Nombre</td>
								<td width="150">
									
								</td>
							</tr>
						</thead>
						<tbody>
							@foreach($cats as $cat)
							<tr>
								<td>
									{!! htmlspecialchars_decode($cat->icono) !!}
								</td>
								<td>
									{{ $cat->name }}
								</td>
								<td>
									<div class="opts">
										<a href="{{url('/admin/category/'.$cat->id.'/edit')}}" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-user-edit"></i></a>
										<a href="{{url('/admin/category/'.$cat->id.'/delete')}}" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="far fa-trash-alt"></i></a>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
						
					</table>

				</div>
				
			</div>
		</div>
		
	</div>
	
</div>
@endsection
@endif