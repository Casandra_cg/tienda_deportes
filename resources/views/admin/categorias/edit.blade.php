@if(kvfj(Auth::user()->permissions, 'categorias'))
@extends('admin.master')
@section('title','Categorias')

@section('breadcrumb')
<li class="breadcrumb-item">
	<a href="{{ url('/admin/categories') }}"><i class="far fa-folder-open"></i> Categorias</a>
</li>

@endsection

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="fas fa-pen"></i>Editar categoria
					</h2>
				</div>
				<div class="inside">
					{!! Form::open(['url' => '/admin/category/'.$cat->id.'/edit']) !!}

					<label for="name">Nombre de la categoria:</label>
					<div class="input-group">
						<span class="input-group-text" id="basic-addon1"><i class="far fa-keyboard"></i></span>

					{!! Form::text('name', $cat->name, ['class' => 'form-control', 'required']) !!}
					</div>

					<label for="module" class="mtop16">Modulo:</label>
					<div class="input-group">
						<span class="input-group-text" id="basic-addon1"><i class="fab fa-product-hunt"></i></span>

					{!! Form::select('module', getModulesArray(), $cat->module , ['class'=> 'form-select']) !!}
					</div>

					<label for="icono" class="mtop16" >Icono:</label>
					<div class="input-group">
						<span class="input-group-text" id="basic-addon1"><i class="fas fa-icons"></i></span>
						{!! Form::text('icono', $cat->icono, ['class' => 'form-control','required']) !!}
					</div>

					{!! Form::submit('Guardar', ['class' => 'btn btn-info mtop16']) !!}
					{!! Form::close() !!}

					@if(Session::has('message'))
				<div class="container">
					<div class="alert alert-{{ Session::get('typealert')}}" style="display:none;">
						{{ Session::get('message')}}
						@if ($errors->any())
						<ul>
							@foreach($errors->all() as $error)
							<li>{{$error}}</li>
							@endforeach
						</ul>
						@endif
						<script>
							$('.alert').slideDown();
							setTimeout(function(){ $('.alert').slideUp();}, 10000);
						</script>
					</div>
				</div>
				@endif
				</div>
			</div>
		</div>

		
		
	</div>
</div>
@endsection
@endif