@if(kvfj(Auth::user()->permissions, 'admin'))
@extends('admin.master')
@section('title','Dashboard')
@section('content')
<div class="container-fluid">
	<div class="panel shadow">
		<div class="header">
			<h2 class="title">
				<i class="fas fa-home"></i> Dashboard
			</h2>
		</div>
		<div class="inside">
			Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic facere maiores eius suscipit veritatis quae sapiente laudantium tempore fugit harum, ea cumque ab iure saepe recusandae esse unde laborum! Ad.
			
		</div>
		
	</div>
</div>
@endsection
@endif
