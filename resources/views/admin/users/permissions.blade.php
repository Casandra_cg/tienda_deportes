@if(kvfj(Auth::user()->permissions, 'usuarios'))
@extends('admin.master')

@section('title','Permisos de usuario')

@section('breadcrumb')
<li class="breadcrumb-item">
	<a href="{{ url('/admin/users') }}"><i class="fas fa-users"></i> Usuarios</a>
</li>
<li class="breadcrumb-item">
	<a href="{{ url('/admin/users') }}"><i class="fas fa-door-open"></i></i> Usuario: {{$u->name}}  (Id: {{$u->id}})</a>
</li>
@endsection
@section('content')
<div class="container-fluid">
		<form action="{{ url('/admin/user/'.$u->id.'/permissions') }}" method="POST" >
			@csrf
			<div class="row">
				@include('admin.users.permissions.module_dashboard')
				@include('admin.users.permissions.module_products')
				@include('admin.users.permissions.module_categories')
				
			</div>
			<div class="row mtop16">
				@include('admin.users.permissions.module_users')
			</div>
			<div class="row mtop16">
				<div class="col-md-12">
					<div class="panel shadow">
						<div class="inside">
							<input type="submit" value="Guardar" class="btn btn-primary">
						</div>
					</div>
				</div>
			</div>
			
		</form>
</div>

@endsection
@endif
