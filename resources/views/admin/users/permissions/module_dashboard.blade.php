	
		<div class="col-md-4 d-flex">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="fas fa-home"></i> Modulo Dashboard
					</h2>
				</div>
				<div class="inside">
					<div class="form-check">
						<input type="checkbox" value="true" name="admin" @if(kvfj($u->permissions, 'admin')) checked @endif ><label for="admin">Puede ver el dashboard</label>
					</div>
					
				</div>
			</div>
		</div>