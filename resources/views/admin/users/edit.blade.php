@if(kvfj(Auth::user()->permissions, 'usuarios'))
@extends('admin.master')

@section('title','Editar Usuarios')

@section('breadcrumb')
<li class="breadcrumb-item">
	<a href="{{ url('/admin/users') }}"><i class="fas fa-users"></i> Usuarios</a>
</li>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
			<div class="col-md-3">
				<div class="panel shadow">
					<div class="header">
						<h2 class="title">
							<i class="fas fa-users"></i> Informacion
						</h2>
					</div>
					<div class="inside">
						<div class="info mtop16">
							<span class="title"><i class="fas fa-users"></i>Nombre:</span>
							
							
						</div>
						<span class="text" >{{ $u->name }} {{ $u->lastname }}</span>
						<div class="info2 mtop16">
							<span class="title"><i class="fas fa-at"></i>Correo Electronico:</span>
							
						</div>
						<span class="text mtop16 ">{{ $u->email }} </span>
						<div class="info3 mtop16">
							<span class="title mtop16"><i class="far fa-calendar-alt"></i>Fecha de registro:</span>
							
						</div>
						<span class="text">{{ $u->created_at }} </span>
						<div class="info4 mtop16">
							<span class="title mtop16"><i class="fas fa-users"></i>Role de usuario:</span>
							
						</div>
						<span class="text">{{ roleUser($u->role ) }} </span>
						
					</div>
				</div>
			</div>
		<div class="col-md-8">
			<div class="panel shadow">
				<div class="header">
					<h2 class="title">
						<i class="fas fa-user-edit"></i> Editar Informacion
					</h2>
				</div>
				<div class="inside">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@endif