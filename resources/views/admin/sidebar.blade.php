<div class="sidebar shadow">
	<div class="section-top">
		<div class="logo">
			<img src="{{url('static/images/logo.jpg')}}" class="img-fluid">
		</div>
		<div class="user">
			<span class="subtitle">Hola:</span>
			<div class="name">
				{{Auth::user()->name}} {{Auth::user()->lastname}}
				<a href="{{url('/logout')}}" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="Salir">
					<i class="fas fa-sign-out-alt"></i> 
				</a>

			</div>
			<div class="email">
				<div class="email">
					{{Auth::user()->email}} 
				</div>
			</div>
		</div>
	</div>
	<div class="main">
		<ul>
			@if(kvfj(Auth::user()->permissions, 'admin'))
			<li>
				<a href="{{ url('/admin') }}" class="lk-admin"><i class="fas fa-home"></i> Dashboard</a>
			</li>
			@endif

			@if(kvfj(Auth::user()->permissions, 'productos'))
			<li>
				<a href="{{ url('/admin/products') }}" class="lk-admin.productos lk-admin.productos.crear lk-admin.productos.crear.P lk-admin.producto.edit lk-admin.producto.editar "><i class="fas fa-boxes"></i> Productos</a>
			</li>
			@endif

			@if(kvfj(Auth::user()->permissions, 'categorias'))
			<li>
				<a href="{{ url('/admin/categories/0') }}" class="lk-admin.productos.categorias lk-admin.categorias.add lk-admin.categorias.edit lk-admin.categorias.editar lk-admin.categorias.eliminar"><i class="far fa-folder-open  "></i> Categorias</a>
			</li>
			@endif

			@if(kvfj(Auth::user()->permissions, 'usuarios'))
			<li>

				<a href="{{ url('/admin/users') }}" class="lk-admin.mostrar.usuarios lk-admin.usuarios.edit"><i class="fas fa-users " ></i> Usuarios</a>
			</li>
			@endif
		</ul>
	</div>

</div>

				
					
					
				
				
		
			