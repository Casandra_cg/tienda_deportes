<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="routeName" content="{{ Route::currentRouteName() }}">
	<script src="../../public/static/js/corazones.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

	<script src="https://kit.fontawesome.com/7c0f4c4dd5.js" crossorigin="anonymous"></script>
	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	
	<script>


		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip()

		});
	</script>

	<link rel="stylesheet"  href="{{ url('/static/css/estilos.css?v='.time()) }}">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
	<title>@yield('title')- Tienda Martin</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg shadow">
		<div class="container-fluid">
			<a class="navbar-brand" href="{{ url('/')}}"> <img src="{{url('static/images/image.jpg')}}" alt=""></a>

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigationMain" aria-controls="navbarSupportedContentContent" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
			<div class="collapse navbar-collapse" id="navigationMain">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a href="{{url('/')}}" class="nav-link"><i class="fas fa-home"></i>Inicio</a>
					</li>
					<li class="nav-item">
						<a href="{{url('/tienda')}}" class="nav-link"><i class="fas fa-store-alt"></i>Tienda</a>
					</li>
					
					<li class="nav-item">
						<a href="{{url('/carrito')}}" class="nav-link"><i class="fas fa-shopping-cart"></i><span class="carnumber"></span></a>
					</li>
					<li class="nav-item">
						<a href="{{url('/logout')}}" class="nav-link" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" title="Salir"><i class="fas fa-sign-out-alt"></i> 
							<span class="carnumber">Cerrar sesion</span></a>
						</a>
					</li>
					@if(Auth::guest())
					<li class="nav-item link-acc">
						<a href="{{url('/login')}}" class="nav-link btn"><i class="far fa-user-circle"></i>Ingresar</a>

						<a href="{{url('/register')}}" class="nav-link btn"><i class="far fa-user-circle"></i>Crear Cuenta</a>
					</li>
					@else
					<li class="nav-item link-acc link-user dropdown">
						<a href="{{url('/login')}}" class="nav-link  btn dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="far fa-user-circle"></i>Hola: {{ Auth::user()->name }}</a>
						 <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						 	<li><a class="dropdown-item" href="#">Action</a></li>
						 </ul>
					</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
	<div class="container-fluid mtop16">
		<div class="row">
		@foreach($products as $p)
			<div class="col-md-3">
				<div class="card">
					<a href="{{ url('/uploads/'. $p->file_path.'/'.$p->image) }} " data-fancybox="gallery">
								<img src="{{ url('/uploads/'. $p->file_path.'/t_'.$p->image) }}" width="100" >
							</a>
					<div class="card-body">
						<span>{{ $p->id }} {{ $p->name }}</span>
						<h5 class="card-title">{{ $p->price }}</h5>
						<a href="{{url('carrito/'.$p->id)}}" class="btn btn-sucess  btn-outline-primary btn-block " role="button"  aria-pressed="true">Agregar al carrito</a>
						
					</div>
				</div>
				
			</div>
			@endforeach
			</div>
	</div>

	
	
</body>
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
</html>
