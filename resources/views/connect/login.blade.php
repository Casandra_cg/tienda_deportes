@extends('connect.master')

@section('title','Login')

@section('content')
<div class="box box_login shadow p-3 mb-5 bg-body rounded">
	<div class="header">
		<a href="{{ url('/') }}">
			<img src="{{ url('static/images/logo.jpg') }}" alt="cfff">
		</a>
	</div>
	<div class="inside">
		{!! Form::open(['url' =>'/login'])!!}

		<label  for="email">Correo Electronico:</label>
		<div class="input-group ">
			<span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
			{!!Form::email('email',null,['class'=>'form-control','required'])!!}
		</div>

		<label class="mtop16" for="password">Contraseña:</label>
		<div class="input-group ">
			<span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
			{!!Form::password('password',['class'=>'form-control','required'])!!}
		</div>
			{!! Form::submit('Ingresar',['class'=>'btn btn-success mtop16'])!!}
			{!! Form::close()!!}
			
			<div class="mtop16">
				<a href="{{ url('/register') }}">¿No tienes una cuenta?, !Registrate¡</a>
				
			</div>
		
		@if(Session::has('message'))
				<div class="container">
					<div class="alert alert-{{ Session::get('typealert')}}" style="display:none;">
						{{ Session::get('message')}}
						@if ($errors->any())
						<ul>
							@foreach($errors->all() as $error)
							<li>{{$error}}</li>
							@endforeach
						</ul>
						@endif
						<script>
							$('.alert').slideDown();
							setTimeout(function(){$('.alert').slideUp();}, 10000);
						</script>
					</div>
				</div>
				@endif		
	</div>
	
</div>


@stop

		
		
		
		
			
	
		