<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\ContentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/',[ContentController::class,'home'])->name('home');
	Route::get('/tienda',[ContentController::class,'tienda'])->name('home');
	Route::get('/carrito',[ContentController::class,'carrito'])->name('carrito');
	Route::get('/carrito/{id}',[ContentController::class,'addcarrito'])->name('carrito');



//routers Auth
Route::get('/login',[UsuarioController::class,'login'])->name('login');
Route::post('/login',[UsuarioController::class,'form_login'])->name('login');
Route::get('/register',[UsuarioController::class,'register'])->name('register');
Route::post('/register',[UsuarioController::class,'form_register'])->name('register');
Route::get('/recover',[UsuarioController::class,'recover'])->name('recover');
Route::get('/logout',[UsuarioController::class,'logout'])->name('logout');