
<?php 
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UsController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoriasController;

Route::prefix('/admin')->group(function(){
	Route::get('/',[AdminController::class,'dashboard'])->name('admin');


	//usuarios
	Route::get('/users',[UsController::class,'user'])->name('usuarios');
	Route::get('/user/{id}/edit',[UsController::class,'editar'])->name('usuarios_edit' );
	Route::get('/user/{id}/permissions',[UsController::class,'permisos'])->name('permissions' );
	Route::post('/user/{id}/permissions',[UsController::class,'editpermisos'])->name('permissions_u' );

	//productoss
	Route::get('/products',[ProductController::class,'productos'])->name('productos');
	Route::get('/product/add',[ProductController::class,'Crear_Producto'])->name('productos_crear');
	Route::post('/product/add',[ProductController::class,'producto'])->name('productos_crear');
	Route::get('/product/{id}/edit',[ProductController::class,'editarp'])->name('producto_edit' );
	Route::post('/product/{id}/edit',[ProductController::class,'editarpro'])->name('producto_edit' );


	//categorias
	Route::get('/categories/{module}',[CategoriasController::class,'home'])->name('categorias');
	Route::post('/category/add',[CategoriasController::class,'category'])->name('categorias_add' );
	Route::get('/category/{id}/edit',[CategoriasController::class,'editar'])->name('categorias_edit' );
	Route::post('/category/{id}/edit',[CategoriasController::class,'editarC'])->name('categorias_edit' );
	Route::get('/category/{id}/delete',[CategoriasController::class,'eliminar'])->name('categorias_eliminar' );

});

 ?>