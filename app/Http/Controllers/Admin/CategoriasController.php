<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator,Str;
 

class CategoriasController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
    	$this->middleware('admin');
    }

    public function home($module){

        $cats= Category::where('module',$module)->orderBy('name', 'Asc')->get();
        $data = ['cats' => $cats];
    	return view('admin.categorias.home', $data);
    }

    public function category(Request $request){
        $rules =[
            'name' => 'required',
            'icono' => 'required',

        ];
        $messages =[
            'name.required' => 'Se requiere un nombre',
            'icono.required' => 'Se requiere un icono'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error. ')->with('typealert','danger');
        else:
            $c = new Category;
            $c->module=$request->input('module');
            $c->name = e($request->input('name')); 
            $c->slug = Str::slug($request->input('name'));
            $c->icono = e($request->input('icono'));
            if($c->save()):
            return back()->with('message','se guardo correctamiente. ')->with('typealert','danger');          
        endif;
        endif;
    }

    public function editar($id){
        $cat = Category::find($id);
        $data = ['cat' => $cat];
        return view('admin.categorias.edit', $data);

    }

        public function editarC(Request $request, $id){
        $rules =[
            'name' => 'required',
            'icono' => 'required',

        ];
        $messages =[
            'name.required' => 'Se requiere un nombre',
            'icono.required' => 'Se requiere un icono'
        ];

        $validator= Validator::make($request->all(), $rules,$messages);
        if ($validator->fails()):
        return back()->withErrors($validator)->with('message','Se ha producido un error. ')->with('typealert','danger');
        else:
            $c = Category::find($id);
            $c->module=$request->input('module');
            $c->name = e($request->input('name')); 
            $c->icono = e($request->input('icono'));
            if($c->save()):

             return back()->with('message','Se guardo. ')->with('typealert','danger');    
        endif;
        endif;
    }

    public function eliminar($id){
        $c = Category::find($id);
        if($c->delete()):
            return back()->with('message','Eliminado con exito. ')->with('typealert','danger');      
        endif;


    }
}
