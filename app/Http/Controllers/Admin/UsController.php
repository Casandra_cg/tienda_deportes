<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UsController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
    	$this->middleware('admin');
    }
    public function user(){
    	$users = User::orderBy('id','Desc')->get();
    	$data =['users' => $users];

    	return view('admin.users.home',$data);


    }
     public function editar($id){
        $u = User::findOrFail($id);
        $data = ['u' => $u];
        return view('admin.users.edit', $data);

    }
    public function permisos($id){
    	$u = User::findOrFail($id);
        $data = ['u' => $u];

    	return view('admin.users.permissions',$data);

    }
    public function editpermisos(Request $request,$id){
    	$u = User::findOrFail($id);
    	$permisos =[
    		'admin' => $request->input('admin'),
            'productos' => $request->input('productos'),
            'productos_crear' => $request->input('productos_crear'),
            'producto_edit' => $request->input('producto_edit'),
            'categorias' => $request->input('categorias'),
            'categorias_add' => $request->input('categorias_add'),
            'categorias_edit' => $request->input('categorias_edit'),
            'categorias_eliminar' => $request->input('categorias_eliminar'),
            'usuarios' => $request->input('usuarios'),
            'usuarios_edit' => $request->input('usuarios_edit'),
            'permissions' => $request->input('permissions'),
            'permissions_u' => $request->input('permissions_u')


    	];
    	$permisos =json_encode($permisos);
    	$u->permissions= $permisos;
        if($u->save()):
            return back();
        endif;

    }
}
