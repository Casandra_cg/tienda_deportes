<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use Validator, Str, Config, Image;

class ProductController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
    	$this->middleware('admin');
    }

    public function productos(){
        $products = Product::orderBy('id','Asc')->paginate(25);
        $data = ['products' => $products];
    	return view('admin.products.home', $data);
    }

    public function Crear_Producto(){

        $cats= Category::where('module', '0')->pluck('name', 'id');
        $data = ['cats'=> $cats];
    	return view('admin.products.crear', $data);

    }

    public function producto(Request $request){
        $rules=[
            'name' => 'required',
            'imagen' => 'required',
            'imagen2' => 'required',
            'price' => 'required',
            'content' => 'required'

        ];

        $messages=[
            'name.required' => 'El nombre del producto es requerido',
            'imagen.required' => 'Debe introducir una imagen',
            'imagen2.required' => 'Debe introducir una imagen',
            'price.required' => 'Ingrese el precio del producto',
            'content.required' => 'Se requiere una descripcion'

        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error. ')->with('typealert','danger')-> withInput();
        else:
            $path = '/'.date('Y-m-d');//  2021-04-03
            $fileExt = trim($request->file('imagen')->getClientOriginalExtension());
            $upload_path = Config::get('filesystems.disks.uploads.root');
            $name= Str::slug(str_replace($fileExt, '', $request->file('imagen')->getClientOriginalName()));
            $filename = rand(1,999).'-'.$name.'.'.$fileExt;
            $file_file = $upload_path.'/'.$path.'/'.$filename;

            $fileExt1 = trim($request->file('imagen2')->getClientOriginalExtension());
            $upload_path1 = Config::get('filesystems.disks.uploads.root');
            $name1= Str::slug(str_replace($fileExt1, '', $request->file('imagen2')->getClientOriginalName()));
            $filename1 = rand(1,999).'-'.$name1.'.'.$fileExt1;
            $file_file1 = $upload_path.'/'.$path.'/'.$filename1;


            $product = new Product;
            $product->status = '0';
            $product->name = e($request->input('name'));
            $product->slug = Str::slug($request->input('name'));
            $product->category_id = $request->input('category');
            $product->file_path = date('Y-m-d');
            $product->image = $filename;
            $product->Image2 = $filename1;
            $product->price = $request->input('price');
            $product->in_discount = $request->input('indiscount');
            $product->discount = $request->input('discount');
            $product->contenido = e($request->input('content'));
            if($product->save()):
                if($request->hasFile('imagen') && $request->hasFile('imagen2')):
                    $fl =$request->imagen->storeAs($path, $filename, 'uploads');
                $img = Image::make($file_file);
                $img->fit(256,256, function($constraint){
                    $constraint->upsize();
                });
                $img->save($upload_path.'/'.$path.'/t_'.$filename);

                $fl1 =$request->imagen2->storeAs($path, $filename1, 'uploads');
                $img1 = Image::make($file_file1);
                $img1->fit(256,256, function($constraint1){
                    $constraint1->upsize();
                });
                $img1->save($upload_path1.'/'.$path.'/t_'.$filename1);
            endif;

             return redirect('/admin/products/')->with('message','Se guardo. ')->with('typealert','succes');    
         endif;

         endif;
    }

    public function editarp($id){
        $p = Product::find($id);
        $cats= Category::where('module', '0')->pluck('name', 'id');
        $data = ['cats'=> $cats, 'p' => $p];
        return view('admin.products.edit', $data);

    }

    public function editarpro($id, Request $request){
        $rules=[
            'name' => 'required',
            'price' => 'required',
            'content' => 'required'

        ];

        $messages=[
            'name.required' => 'El nombre del producto es requerido',
            'price.required' => 'Ingrese el precio del producto',
            'content.required' => 'Se requiere una descripcion'

        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error. ')->with('typealert','danger')-> withInput();
        else:
            

            $product =  Product::find($id);
            $product->status = $request->input('status');
            $product->name = e($request->input('name'));
            $product->category_id = $request->input('category');
            if($request->hasFile('imagen') && $request->hasFile('imagen2')):
            $product->$file_path = date('Y-m-d');//  2021-04-03
            $product->image = $filename;
            $product->image2 = $filename1;

             endif;
            $product->price = $request->input('price');
            $product->in_discount = $request->input('indiscount');
            $product->discount = $request->input('discount');
            $product->contenido = e($request->input('content'));
            if($product->save()):
                if($request->hasFile('imagen') && $request->hasFile('imagen2')):
                    $fl =$request->imagen->storeAs($path, $filename, 'uploads');
                $img = Image::make($file_file);
                $img->fit(256,256, function($constraint){
                    $constraint->upsize();
                });
                $img->save($upload_path.'/'.$path.'/t_'.$filename);

                $fl1 =$request->imagen2->storeAs($path, $filename1, 'uploads');
                $img1 = Image::make($file_file1);
                $img1->fit(256,256, function($constraint1){
                    $constraint1->upsize();
                });
                $img1->save($upload_path.'/'.$path.'/t_'.$filename1);
            endif;

             return back()->with('message','Se guardo. ')->with('typealert','succes');    
         endif;

         endif;

    }
}
