<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ContentController extends Controller
{
    public function home(){

    	return view('master');

    }
    public function tienda(){
    	 $products = Product::orderBy('id','Asc')->paginate(25);
        $data = ['products' => $products];
    	return view('product',$data);

    }

    public function carrito(){

    	
    	
    	return view('carrito');
    	
    }

    public function addcarrito($id){
    	$product = Product::find($id);
    	$cart = session()->get('cart');

    	if (!$cart) {
    		$cart= [
    			$id => [
    				'name'=> $product->name,
    				'quantity'=> 1,
    				'price'=> $product->price,
    				'image'=> $product->image
    			]
    		];

    		session()->put('cart',$cart);
    		return redirect()->back()->with('sucess','Producto agregado al carrito');

    		
    	}

    	if (isset($cart[$id])) {
    		$cart[$id]['quantity']++;
    		session()->put('cart',$cart);

    		return redirect()->back()->with('sucess','Producto agregado al carrito');
    	}

    	$cart[$id]=[
    		'name'=> $product->name,
    		'quantity' => 1,
    		'price' =>$product->price,
    		'image'=> $product->image
    	];
    	session()->put('cart', $cart);
    	return redirect()->back()->with('sucess','Producto agregado al carrito');
    	
    }


}
