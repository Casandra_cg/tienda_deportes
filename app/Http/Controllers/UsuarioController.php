<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Hash, Auth;
use App\Models\User;
class UsuarioController extends Controller
{
	public function __Construct(){
		$this->middleware('guest')->except(['logout']);
	}
	public function login (){
		return view('connect.login');
	}
	public function register (){
		return view('connect.register');
	}
	public function form_login(Request $request){
		$rules=[
			'email'=> 'required|email',
			'password'=> 'required|min:8',

		];
		$messages=[
			'email.required'=>'Su correo es requerido.',
			'email.email'=>'El formato de su correo es incorrecto.',
			'password.required'=>'No introducio su contraseña.',
			'password.min'=>'La contraseña debe de tener minimo 8 caracteres.',
		];
		$validator= Validator::make($request->all(), $rules,$messages);
		if ($validator->fails()):
		return back()->withErrors($validator)->with('message','Se ha producido un error. ')->with('typealert','danger');
		else:
			if(Auth::attempt(['email'=>$request->input('email'), 'password'=>$request-> input('password')],true)):
				return redirect('/');

			else:
				return back()->with('message','El correo o la contraseña es incorrecta. ')->with('typealert','danger');
			endif;

		endif;
	}

	public function form_register(Request $request){

		$rules=[
			'name' => 'required',
			'lastname' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:8',
			'cpassword' => 'required|min:8|same:password'
		];
		$messages=[
			'name.required'=>'Su nombre es requerido.',
			'lastname.required'=>'Su apellido es requerido.',
			'email.required'=>'Su correo es requerido.',
			'email.email'=>'El formato de su correo es incorrecto.',
			'email.unique'=>'Su coreo ya existe.',
			'password.required'=>'No introducio su contraseña.',
			'password.min'=>'La contraseña debe de tener minimo 8 caracteres.',
			'cpassword.required'=>'Es necesario confirmar la contraseña.',
			'cpassword.min'=>'La contraseña debe de tener minimo 8 caracteres.',
			'cpassword.same'=>'Las contraseñas no coinciden.',
		];

		$validator= Validator::make($request->all(), $rules,$messages);
		if ($validator->fails()):
		return back()->withErrors($validator)->with('message','Se ha producido un error. ')->with('typealert','danger');
		else:
			$user = new User;
			$user->name = e($request->input('name'));
			$user->lastname = e($request->input('lastname'));
			$user->email = e($request->input('email'));
			$user->password = Hash::make($request->input('password'));

			if($user->save()):
				return redirect('/login')->with('message','Su registro fue exitoso. ')->with('typealert','success');
		endif;
		endif;
			
		
	}

	public function logout(){
		 Auth::logout();
		 return redirect('/');
	}
    
}
