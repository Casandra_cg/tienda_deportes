-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-04-2021 a las 09:46:17
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `module`, `name`, `slug`, `icono`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'Balones', 'balones', '&lt;i class=&quot;fas fa-baseball-ball&quot;&gt;&lt;/i&gt;', NULL, '2021-04-07 10:33:31', '2021-04-07 10:33:31'),
(2, 0, 'Playeras', 'playeras', '&lt;i class=&quot;fas fa-tshirt&quot;&gt;&lt;/i&gt;', NULL, '2021-04-07 10:34:58', '2021-04-07 10:34:58'),
(3, 0, 'ball', 'ball', '&lt;i class=&quot;fas fa-baseball-ball&quot;&gt;&lt;/i&gt;', NULL, '2021-04-07 10:35:50', '2021-04-07 10:35:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_04_03_054701_create_categories_table', 2),
(5, '2021_04_03_205308_create-products_table', 3),
(6, '2021_04_04_051742_add_field_file_path_to_products_table', 4),
(7, '2021_04_04_052913_field_file_path_to_products_table', 5),
(8, '2021_04_04_210427_add_field_permissions_to_users_table', 6),
(9, '2021_04_04_210810_add_file_permissions_to_users_table', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `in_discount` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `status`, `name`, `slug`, `category_id`, `file_path`, `image`, `Image2`, `price`, `in_discount`, `discount`, `contenido`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'Balon de Futbol', 'balon-de-futbol', 1, '2021-04-07', '555-balon-futbol.jpg', '172-balon-futbol.jpg', '200.00', 0, 0, 'Balon de futbol color neon', NULL, '2021-04-07 10:36:56', '2021-04-07 10:36:56'),
(2, 0, 'Balon_basquetboll', 'balon-basquetboll', 1, '2021-04-07', '50-balos-basquet.jpg', '756-balos-basquet.jpg', '230.00', 0, 0, 'Balon de basquetbol color naranja', NULL, '2021-04-07 10:37:38', '2021-04-07 10:37:38'),
(3, 0, 'Pelota de golf', 'pelota-de-golf', 3, '2021-04-07', '858-pelota-de-golf.jpg', '76-pelota-de-golf.jpg', '100.00', 0, 0, 'Pelota de golf color blanca', NULL, '2021-04-07 10:38:35', '2021-04-07 10:38:35'),
(4, 0, 'Pelota de tenis', 'pelota-de-tenis', 3, '2021-04-07', '356-pelota-de-tenis.jpg', '618-pelota-de-tenis.jpg', '220.00', 0, 0, 'Pelota de tenis neon', NULL, '2021-04-07 10:39:29', '2021-04-07 10:39:29'),
(5, 0, 'Playera del guadalajara', 'playera-del-guadalajara', 2, '2021-04-07', '859-playera-del-guadalajara.jpg', '487-playera-del-guadalajara.jpg', '1000.00', 0, 0, 'Playera del quipo de guadalajara', NULL, '2021-04-07 10:40:22', '2021-04-07 10:40:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `lastname`, `email`, `email_verified_at`, `password`, `permissions`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Casandra', 'Ceballos Garza', 'cass@gmail.com', NULL, '$2y$10$ob6AjxipTtq5HUVvrZJLdeqe75J1lC9MLPu5kAeUx5z9P3614tYFe', '{\"admin\":\"true\",\"productos\":\"true\",\"productos_crear\":null,\"producto_edit\":null,\"categorias\":\"true\",\"categorias_add\":null,\"categorias_edit\":null,\"categorias_eliminar\":null,\"usuarios\":\"true\",\"usuarios_edit\":null,\"permissions\":null,\"permissions_u\":null}', 'K0XdnJlrDLAN7IOr68A5ewvohkzVJsM5iKt2vIbS1pxL7xuU43qqOVR76zrt', '2021-04-02 05:49:26', '2021-04-05 12:40:37'),
(2, 0, 'Casandra', 'Ceballos Garza', 'aidgarza00@gmail.com', NULL, '$2y$10$6bkuTaVaWxtEe26Y6Q3Tgequw6wt7DDcfxtw8DCDO0yMey6NvAKcy', '{\"admin\":\"true\",\"productos\":\"true\",\"productos_crear\":null,\"producto_edit\":null,\"categorias\":\"true\",\"categorias_add\":null,\"categorias_edit\":null,\"categorias_eliminar\":null,\"usuarios\":\"true\",\"usuarios_edit\":null,\"permissions\":null,\"permissions_u\":null}', NULL, '2021-04-04 04:32:56', '2021-04-05 12:32:52'),
(3, 0, 'Casandra', 'Ceballos Garza', 'joshua@gmail.com', NULL, '$2y$10$taCIpYso5zBEkYE0r91Md.qDgWIAMTS5.MP3lCL.lJnF8p0xHTG2G', NULL, 'xGeaA0mEZjvUVxoUU3ssMlne2mPK9de5zmjoY23B57r5QZYKJ68aVbuxcC3y', '2021-04-07 10:43:35', '2021-04-07 10:43:35');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
